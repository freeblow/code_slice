
// @dart=2.9
import 'package:xxx/macro/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final String title;
  final Function onPress;
  final double width;
  final double height;
  final bool enable;
  const PrimaryButton(this.title, {Key key,this.onPress,this.width,this.height,this.enable = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: (this.enable != null && this.enable) ? onPress : () => {},
      child:  Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          color: (this.enable != null && this.enable) ? KColor.RED : KColor.RED_DISABLE
        ),
        child: Center(
          child: Text(title,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18,
                color: KColor.WHITE,
              ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class RaisedButton extends StatelessWidget {
  final Widget child;
  final EdgeInsets? padding;
  final OutlinedBorder shape;
  final Color color;
  final Function() onPressed;
  final double? elevation;
  final Color? disabledColor;

  const RaisedButton({
    Key? key,
    required this.child,
    required this.shape,
    required this.color,
    required this.onPressed,
    this.elevation,
    this.padding,
    this.disabledColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: child,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(color),
        elevation: MaterialStateProperty.all(elevation),
        padding: MaterialStateProperty.all(padding),
        shape: MaterialStateProperty.all(shape),
      ),
      onPressed: onPressed,
    );
  }
}

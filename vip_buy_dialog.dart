import 'package:xx/routers/fluro_navigator.dart';
import 'package:xx/utils/application.dart';
import 'package:xx/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xx/utils/image_utils.dart';
import 'package:video_player/video_player.dart';

import '../macro/color.dart';
import '../network/dio_utils.dart';
import '../pages/Splash/splash_web.dart';
import 'cachedImageUtil.dart';
import 'common_fun.dart';
import 'lifecycle_simple_dialog.dart';
import 'dart:io';

VideoPlayerController? videoPlayerController;

/*
type: 3:权益绑定,2:匿名成功,1:实名成功
title: 标题
credits: 获得的积分
price: 购买的价格
* */
void showVipBuyOkDialog(BuildContext context, String type, String title, String credits, String price,
    {bool dismissOutSideTouch = false,
    VoidCallback? loginCall,
    VoidCallback? checkCreditsCall,
    VoidCallback? cancelCall,
    bool willPopWrapper = false,
    String? traceShowCode}) async {
  showDialog(
      context: context,
      barrierDismissible: dismissOutSideTouch,
      builder: (BuildContext context) {
        LifeCycleSimpleDialog simpleDialog = LifeCycleSimpleDialog(
          traceShowCode: traceShowCode,
          backgroundColor: Colors.transparent,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          contentPadding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          children: <Widget>[
            Container(
              width: 274,
              height: type == "1" ? 308 : 338,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: ImageUtils.getAssetImage(
                          type == "1" ? "vip/real_name_purchase_bg" : "vip/anonymous_purchase_bg"))),
              child: Padding(
                padding: const EdgeInsets.only(left: 12, top: 28, right: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 7),
                      child: Image(
                        image: ImageUtils.getAssetImage(
                            type == "3" ? "vip/vip_binding_title_img" : "vip/purchase_sucess_title"),
                        width: 136,
                        height: 47,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 7),
                      child: Container(
                        alignment: Alignment.centerLeft,
                        height: 40,
                        child: Text(
                          type == "1" ? intl("xx") : intl("xxx"),
                          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18, height: 1),
                        ),
                      ),
                    ),
                    Container(
                      width: 250,
                      height: type == "1" ? 176 : 210,
                      decoration:
                          BoxDecoration(color: KColor.WHITE, borderRadius: BorderRadius.all(Radius.circular(22))),
                      child: Padding(
                        padding: EdgeInsets.only(top: (type == "1" ? 27 : 15)),
                        child: Column(
                          children: [
                            Container(
                              width: 202,
                              height: 110,
                              decoration: BoxDecoration(
                                  image:
                                      DecorationImage(image: ImageUtils.getAssetImage("vip/purchase_sucess_inner_bg"))),
                              child: Stack(
                                children: [
                                  Positioned(
                                    top: 9,
                                    left: 12,
                                    child: Text(intl("xxx"),
                                        style: TextStyle(
                                            fontSize: 16, fontWeight: FontWeight.w600, color: Color(0xFF7E5022))),
                                  ),
                                  Positioned(
                                    top: 32,
                                    left: 12,
                                    child: Text(intl("xxx", params: [credits]),
                                        style: TextStyle(
                                            fontSize: 24, fontWeight: FontWeight.w600, color: Color(0xFFFB4B42))),
                                  ),
                                  Positioned(
                                    bottom: 3,
                                    left: 12,
                                    right: type == "1" ? 70 : 8,
                                    child: Container(
                                      // width: 120,
                                      height: 30,
                                      alignment: Alignment.centerLeft,
                                      child: Text(intl("xxx", params: [price]),
                                          maxLines: 2,
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              height: 1,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                              color: Color(0xFF7E5022))),
                                    ),
                                  ),
                                  if (type == "1")
                                    Positioned(
                                      bottom: 6,
                                      right: 8,
                                      child: GestureDetector(
                                        onTap: () {
                                          NavigatorUtils.goBack(context);
                                          if (checkCreditsCall != null) {
                                            checkCreditsCall();
                                          }
                                        },
                                        child: Container(
                                          width: 60,
                                          height: 24,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(Radius.circular(24)),
                                              gradient: LinearGradient(
                                                colors: [Color(0xFFFF5230), Color(0xFFFF7D29)],
                                                begin: Alignment.centerLeft,
                                                end: Alignment.centerRight,
                                              )),
                                          child: Center(
                                            child: Text(intl("xxx"),
                                                style: TextStyle(
                                                    fontSize: 13, fontWeight: FontWeight.w600, color: KColor.WHITE)),
                                          ),
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 12,
                            ),
                            Text(intl("xxx"),
                                style: TextStyle(fontSize: 11, fontWeight: FontWeight.w400, color: Color(0xFFBA7E25))),
                            if (type != "1")
                              SizedBox(
                                height: 12,
                              ),
                            if (type != "1")
                              GestureDetector(
                                onTap: () {
                                  NavigatorUtils.goBack(context);
                                  if (loginCall != null) {
                                    loginCall();
                                  }
                                },
                                child: Container(
                                  width: 178,
                                  height: 36,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(9)),
                                      gradient: LinearGradient(
                                        colors: [Color(0xFFFF443D), Color(0xFFFF8C3C)],
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                      )),
                                  child: Center(
                                    child: Text(intl("xxx"),
                                        style:
                                            TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: KColor.WHITE)),
                                  ),
                                ),
                              )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Container(
              // padding: EdgeInsets.only(bottom: 20),
              child: Center(
                child: IconButton(
                  onPressed: () {
                    NavigatorUtils.goBack(context);
                    if (cancelCall != null) {
                      cancelCall();
                    }
                  },
                  icon: Image.asset(ImageUtils.getImgPath("ad_close_btn")),
                ),
              ),
            )
          ],
        );
        if (willPopWrapper) {
          return new WillPopScope(child: simpleDialog, onWillPop: () async => false);
        } else {
          return simpleDialog;
        }
      });
}

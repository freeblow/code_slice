import 'package:flutter/material.dart';

class FlatButton extends StatelessWidget {
  final Widget child;
  final Function() onPressed;
  final EdgeInsets? padding;
  final ButtonStyle? style;

  const FlatButton(
      {Key? key,
      required this.child,
      required this.onPressed,
      this.padding,
      this.style})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Padding(
        padding: padding ?? EdgeInsets.zero,
        child: child,
      ),
      style: style,
      onPressed: onPressed,
    );
  }
}

// @dart=2.9
import 'package:xxx/macro/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nuts_activity_indicator/nuts_activity_indicator.dart';

/// 加载中的弹框
class ProgressDialog extends Dialog {
  const ProgressDialog({
    Key key,
    this.hintText: '',
  }) : super(key: key);

  final String hintText;

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: Container(
          height: 88.0,
          width: 120.0,
          alignment: Alignment.center,
          decoration: ShapeDecoration(
              color: KColor.hudBackgroundColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(6.0)))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
//              Theme(
//                data: ThemeData(
//                    cupertinoOverrideTheme: CupertinoThemeData(
//                        brightness: Brightness.dark // 局部指定夜间模式，加载圈颜色会设置为白色
//                        )),
//                child: CupertinoActivityIndicator(radius: 10.0),
//              ),


          NutsActivityIndicator(
            radius: 12.0,
            activeColor: KColor.hudForegroundColor,
          ),
              // CupertinoActivityIndicator(radius: 12.0),

              Offstage(
                  offstage: hintText.isEmpty,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(0, 12, 0, 0),
                    child: Text(
                      hintText,
                      style: TextStyle(color: KColor.hudForegroundColor, fontSize: 13),
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

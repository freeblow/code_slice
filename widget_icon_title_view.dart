// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../macro/color.dart';

class IconTitleView extends StatelessWidget {
  final String title;
  final Widget trailing;
  final ValueChanged<String> onItemClick;
  final Color backgroundColor;
  final Color fontColor;
  final bool enableInkWell; //是否支持点击水印效果

  IconTitleView(this.title, this.trailing,
      {this.onItemClick,
      this.backgroundColor = Colors.white,
      this.enableInkWell = false,
      this.fontColor = KColor.COLOR_PRIMARY});

  @override
  Widget build(BuildContext context) {
    return enableInkWell
        ? Material(
            color: backgroundColor,
            child: InkWell(
              onTap: () {
                if (onItemClick != null) {
                  onItemClick(title);
                }
              },
              child: _buildBody(),
            ),
          )
        : GestureDetector(
            onTap: () {
              if (onItemClick != null) {
                onItemClick(title);
              }
            },
            child: Container(
              color: backgroundColor,
              child: _buildBody(),
            ),
          );
  }

  Widget _buildBody() => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Align(
            child: Text(
              title,
              style: TextStyle(color: fontColor, fontSize: 15),
            ),
          ),
          Spacer(),
          trailing,
        ],
      );
}

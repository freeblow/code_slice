// @dart=2.9
import 'package:xxx/macro/color.dart';
import 'package:FlutterChenYin/utils/image_utils.dart';
import 'package:flutter/material.dart';

class SquareTextList extends StatelessWidget {
  final List<String> textList;

  SquareTextList({Key key, this.textList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: Color(0xFFEBEBEB),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 12),
        child: Wrap(
          spacing: 0,
          runSpacing: 8,
          runAlignment: WrapAlignment.center,
          children: getWrapChildren(),
        ),
      ),
    );
  }

  List<Widget> getWrapChildren() {
    List<Widget> children = [];
    children.add(Container(
      width: 12,
    ));
    children.add(Image(
      image: ImageUtils.getAssetImage("xx/multiple_values_text_mark_gray"),
      width: 18,
      height: 18,
    ));
    for (int i = 0; i < textList.length; i++) {
      Widget text = Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: Text(
          textList[i],
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: KColor.COLOR_595959),
        ),
      );
      children.add(text);
      if (i != textList.length - 1) {
        Widget separator = Container(
          width: 1,
          height: 16,
          color: KColor.COLOR_B9B9B9,
        );
        children.add(separator);
      }
    }
    return children;
  }
}
